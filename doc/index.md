# `ircamt`

Some helper tools to work with thermographic images.

To get a feeling for basic steps after porting thermographic images to more reasonable
array like formats ave a look at the [example notebook](example.ipynb).

The package provides three main modules with different functionality. Each of
them (unfortunately) depends on an extenal package. I made these dependencies
optional. So, depending on the nneeded functionality they can be installed separately.


## Module `convert_flir`

Convert IR-JPEG files created by FLIR cameras to 2D arrays of temperature data. Currently,
implemented formats are a `csv` (data) plus `JSON` (metadata) combination, and MATLAB `.mat`.
There are two main functions
  1. `convert_file` converts a thermographic image(e.g. jpeg) to csv or mat array formats. For example:
     ```python
     ircamt.convert_file('example.jpg', output='both', show_warnings=False)
     ```
     will create the files `example.csv`, `example.json` and `example.mat`. If the parameter `use_folder`
     is `True`, output folders for each format will be created.
  2. `convert_all_in_folder` converts all thermographic jpeg images in a folder to one or both output
     formats. If a bunch of such jpeg files is located in folder `./jpegs` the call
     ```python
     ircamt.convert_all_in_folder('./jpegs', output='both', show_warnings=False)
     ```
     will create the format folders `csv_json` and `mat` and place converted files in there. The
     function will grab all but one of the local machine's advertised CPU threads. This number may
     also be specified directly with the `nprocs` parameter.


## Module `camera`

Given a number of matching camera (2D) and 'world' (3D) coordinates, this module helps transforming
between the two coordinate systems.

### `Camera`

`Camera` is an object that stores information about a camera, such as focal length sensor size and
sensor pitch. From that the intrinsic matrix of a pinhole camera model is created which is
necessary to transform to a world coordinate system.

One subclass is implemented for a specific camera model (an old FLIR Systems ThemaCam 640).
It is fairly straight forward to create more of such subclasses for other cameras. A camera
instance them specifies the lense's focal length (in mm), for exaample for a 40 mm lense:
```python
cam = ircamt.ThermacamSc640(f_mm=40)
```


### `Scene`

The `Scene` class allows to transform between world and camera coordinate systems. To do so it
uses a `Camera` instance, a rotation and a translation in vector or matrix form.

Implemented transform functions:

- `Scene.world_to_cam`
- `Scene.cam_to_worldplain`
- `Scene.cam_to_worldpoint`


