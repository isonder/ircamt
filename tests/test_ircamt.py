import unittest
import datetime
from pathlib import Path
import numpy as np

import ircamt


class TimelineTestCase(unittest.TestCase):
    def test_datetime_from_flirstr(self):
        """Testing if time stamps are parsed correctly"""
        dts = "2023:05:27 14:11:37.523-05:00"
        correct = datetime.datetime(2023, 5, 27, 14, 11, 37, 523000)
        self.assertEqual(
            correct,
            ircamt.datetime_from_flirstr(dts),
        )


class ProcessTemperaturesTestCase(unittest.TestCase):
    """Tests of the `process_temperatures` module."""

    corners_trap = np.array(
        [
            [0, 0, 0],
            [2, 0, 0],
            [2, 1, 0],
            [0, 1, 0],
            [0, 0, 0],
            [0, 2, 0],
            [0, 2, 1],
            [0, 0, 1],
        ]
    )
    corners_triag = corners_trap[[0, 1, 2, 4, 5, 6]]

    def test_areas_from_corners_traps(self):
        """Test for correct return values of `areas_from_corners()` for
        trapezoidal area shapes.
        """
        correct_trap = np.array([2.0, 2.0])
        self.assertTrue(
            np.all(
                [
                    np.isclose(a, b)
                    for a, b in zip(
                        correct_trap, ircamt.area_from_corners(self.corners_trap)
                    )
                ]
            )
        )

    def test_areas_from_corners_triang(self):
        """Test for correct return values of `areas_from_corners()` for
        triangular area shapes.
        """
        correct = np.array([1.0, 1.0])
        self.assertTrue(
            np.all(
                [
                    np.isclose(a, b)
                    for a, b in zip(
                        correct,
                        ircamt.area_from_corners(self.corners_triag, shape="triangle"),
                    )
                ]
            )
        )

    def test_sequence_plot(self):
        """Test creation of a valid html file output of `sequence_plot()`."""
        folder = Path(__file__).absolute().parent.parent / "data/example_data/csv_json"
        csvs = list(folder.glob("*.csv"))
        if len(csvs) == 0:
            pfolder = folder.parent.parent
            from urllib.request import urlretrieve
            urlretrieve(
                "https://zenodo.org/records/14932010/files/example_data.zip?download=1",
                pfolder / "example_data.zip"
            )
            from zipfile import ZipFile
            with ZipFile(pfolder / "example_data.zip") as ziprf:
                ziprf.extractall(pfolder)
            csvs = list(folder.glob("*.csv"))
        print(f"{folder=}, {csvs=}", flush=True)
        ircamt.sequence_plot(csvs, "Test Sequence Plot")


if __name__ == "__main__":
    unittest.main()
