Some helper tools to work with thermographic images.

The package provides three main modules with different functionality. Each of
them (unfortunately) depends on an extenal package. I made these dependencies
optional. So, depending on the nneeded functionality they can be installed separately.

- `convert_flir` (depends on [`FlirImageExtractor`](https://github.com/nationaldronesau/FlirImageExtractor)):  
  Provides functions to batch convert sets of many thermographic JPEG images to
  2D arrays of temperatures and store them in a more convenient format, such as
  `csv` or `mat`. This module builds on the `FlirImageExtractor` package and
  mass-applies their converter to a given list of images.
- `camera` (depends on [`opencv`](https://github.com/opencv/opencv-python)):  
  Helps to transform 2D camera coordinates to 3D real space or
  vice versa, given that sufficient information is available. This sets up a
  pinhole camera model and uses `opencv`
  to transform from 3D to 2D coordinates. To go in the other direction, from
  2D to 3D, some assumptions are necessary. Two functions are provided so far:
  One assumes the distance from camera to object is known. The other assumes
  points of interest lie in a plain which is given by a point and a normal
  vector.
- `process_temperatures` (depends on [`twodpg`](https://gitlab.com/isonder/twodpg)):  
  Provides convenience functions to extract a timeline from a set of images, or
  to apply a fairly arbitrary analysis function to a subset of each image's
  pixels. Also, functions are provided to integrate a scalar field over a
  (surface) area which has to be provided as triangles or trapezoids.


## Installation

This should be installed into a virtual environment. So, create one and enter.

If one of the dependncies below is too ugly to install, the dependency check
can be omitted by removing the `['all']` in the last installation step. Then,
depending on which dependency is not installed one module will not work, but the
others will. Then, dependency installation may be skipped.

Before cloning this repo, install two dependencies manually:
1. `FlirImageExtractor`: This should install automatically. However, their
   dependency list is unnecessarily outdated. So, clone their repository
   ```bash
   git clone https://github.com/nationaldronesau/FlirImageExtractor.git
   ```
   and change all the dependencies in `pyproject.toml` (in the `tool.poetry.dependencies`)
   section to have `>=` instead of `==` signs.
2. `twodpg`: This is necessary to select image pixels from polygons efficiently.
  head over to its homepage at
  [`gitlab.com/isonder/twodpg`](https://gitlab.com/isonder/twodpg) and follow
  the installation instructions.

Now download this repo or clone it with git. Then install the package in the
virtual environment. This will install the remaining dependencies if necessary
automatically.
```bash
git clone https://gitlab.com/isonder/ircamt.git
cd ircamt
pip install -e .['all']
```


## Example Usage

There is also an example [Jupyter notebook](doc/example.ipynb).

```python
import ircamt as ir

# get a list of times and corresponding files
tl, files = ir.make_timeline('some folder/where my/flir files/are')

# select a suitable subset (e.g. initial frames are not interesting)
t = tl[69:]
fs = files[69:]

# Define some areas of interest similar to polygon vertices (x, y) values
rect1 = [[300, 164], [300, 220], [360, 220], [360, 164]]
rect2 = [[250, 270], [250, 350], [320, 350], [320, 270]]

# Get maximum in rect1 and rect2 over time
max1 = ir.get(what=ir.max_temp, files=fs, sel=rect1, nrvals=3)
max2 = ir.get(ir.max_temp, fs, rect2, 3)
```
