import asyncio
import threading
import queue
import multiprocessing as mp
import warnings
import platform
from typing import Callable

# NPROCS and set_processing need to be defined first, since these are imported
# by the ircamt_proj module.
NPROCS: int = mp.cpu_count()

# This is a not so elegant solution to the problem that the multiprocessing
# package does not really work on all platforms.
_KNOWN_PLTFS = ("windows", "linux", "darwin")
_PROC_CHOICES = ("process", "proc", "subproc", "subprocess")


def set_processing(pwish: str = None, force_wish: bool = False):
    if force_wish and pwish is None:
        raise ValueError(f"Cannot force None type processing backend.")
    else:
        pltf = platform.system().lower()
        if pltf not in _KNOWN_PLTFS:
            if pwish is not None:
                # Warn if processes were requested on unknown platform, otherwise
                # switch to threads silently.
                if pwish.lower() in _PROC_CHOICES:
                    warnings.warn(
                        f"Cannot use multiprocesing on this platform ({pltf}) "
                        f"for this task. Using threads instead."
                    )
            pwish = "thread"
        elif pltf == "windows":
            if pwish is None:
                pwish = "thread"
            elif pwish.lower() in _PROC_CHOICES:
                if not force_wish:
                    warnings.warn(
                        f"Cannot use multiprocesing on this platform ({pltf}) "
                        f"for this task. Using threads instead."
                    )
                    pwish = "thread"
            else:
                pwish = "thread"
        else:
            if pwish is None:
                pwish = "process"
    if pwish.lower() in _PROC_CHOICES:
        ptype, qtype = mp.Process, mp.Queue
    else:
        ptype, qtype = threading.Thread, queue.Queue
    return ptype, qtype


def get_aiorun() -> Callable:
    """Depending on the platform in use, this returns either `asyncio.run`
    (for Linux or Mac) or `asyncio.new_event_loop().run_until_complete`
    (for Windows).
    """
    pltf = platform.system().lower()
    if pltf not in _KNOWN_PLTFS:
        warnings.warn(
            f"Platform '{pltf}' not recognized. Assuming this platform is Linux "
            f"compatible."
        )
        pltf = "linux"
    if pltf in ("linux", "darwin"):
        return asyncio.run
    else:
        return lambda arg: asyncio.new_event_loop().run_until_complete(arg)
