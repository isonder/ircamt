"""Convert IR-JPEG files created by FLIR cameras to arrays of temperature data.
"""
from flirimageextractor import FlirImageExtractor
from scipy.io import savemat
import numpy as np
import json
from pathlib import Path
from typing import Union, TypedDict, Tuple
import warnings
import asyncio
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor

from ircamt.set_processing import NPROCS, get_aiorun

aiorun = get_aiorun()

str_or_path = Union[str, Path]
IGNORE_EXPR = r"^\binvalid value encountered in log\b"
DEBUG: bool = False


class OutDict(TypedDict):
    mat: str_or_path
    text: str_or_path


def _handle_folders(fld: str_or_path, output: str, use_folder: bool = True) -> tuple:
    fld = Path(fld)
    if not fld.exists():
        raise ValueError(f"Input path not found: {fld}")
    if not isinstance(output, list):
        output = ["mat", "text"] if output == "both" else [output]
    if use_folder:
        matfold = fld.parent / "mat" if "mat" in output else None
        txtfold = fld.parent / "csv_json" if "text" in output else None
    else:
        matfold = fld if "mat" in output else None
        txtfold = fld if "text" in output else None
    if matfold is not None:
        matfold.mkdir(exist_ok=True)
    if txtfold is not None:
        txtfold.mkdir(exist_ok=True)
    return fld, matfold, txtfold


def _proc_img(pth, extr: FlirImageExtractor, show_warnings=True) -> tuple:
    print(pth)
    with warnings.catch_warnings():
        if not show_warnings:
            warnings.filterwarnings("ignore")
        extr.process_image(pth)
        data = extr.thermal_image_np
    meta = extr.get_metadata(pth)
    return meta, data


def _dump_json(jsn: str, pth: Path):
    with open(pth, mode="w") as fp:
        json.dump(jsn, fp)


async def conv_file(
    loop: asyncio.AbstractEventLoop,
    pth: Path,
    extr: FlirImageExtractor,
    output: OutDict,
    procex: ProcessPoolExecutor,
    threx: ThreadPoolExecutor,
    show_warnings: bool = True,
):
    if DEBUG:
        print(f"conv_file: {output=}")
    meta, data = await loop.run_in_executor(procex, _proc_img, pth, extr, show_warnings)
    if "mat" in output.keys() or output["mat"] is not None:
        loop.run_in_executor(
            threx,
            savemat,
            *(output["mat"] / f"{pth.stem}.mat",),
            {"mdict": {"temperature": data, "metadata": meta}, "do_compression": True},
        )
    if "text" in output.keys() or output["text"] is not None:
        loop.run_in_executor(
            threx, np.savetxt, *(output["text"] / f"{pth.stem}.csv", data, "%.5e", ",")
        )
        loop.run_in_executor(
            threx, _dump_json, *(meta, output["text"] / f"{pth.stem}.json")
        )


# For whatever reason I cannot define this inside convert_file(). Maybe, in a
# couple of years, asyncio will learn how to do this better.
async def _convfile_bridge(fname, extr, out, show_warnings):
    loop = asyncio.get_event_loop()
    await conv_file(
        loop,
        fname,
        extr,
        out,
        ProcessPoolExecutor(),
        ThreadPoolExecutor(),
        show_warnings,
    )


def convert_file(
    fname: str_or_path,
    output: str = "both",
    show_warnings: bool = True,
    use_folder: bool = False,
):
    """

    Parameters
    ----------
    fname : str or Path
        File to convert. Must be a .jpg file
    output : str, optional
        One of 'mat', 'text', 'both'; for Matlab files, csv + JSON, or both
        of those two.
    show_warnings : bool, optional
        Whether to show warnings of the conversion process.
    use_folder : bool, optional
        Whether to create folders for the chosen output type(s). Default is False.

    Returns
    -------
        Returns nothing, but creates data files on the hard drive.
    """
    fname = Path(fname)
    folder, matfold, txtfold = _handle_folders(fname.parent, output, use_folder)
    out: OutDict = {"mat": matfold, "text": txtfold}
    if DEBUG:
        print(f"convert_file: {out=}")
    extr = FlirImageExtractor()
    aiorun(_convfile_bridge(fname, extr, out, show_warnings))


async def convert_all(
    folder: str_or_path,
    output: str = "both",
    show_warnings: bool = True,
    nprocs: int = NPROCS,
):
    folder, matfold, txtfold = _handle_folders(folder, output)
    out: OutDict = {"mat": matfold, "text": txtfold}
    extr = FlirImageExtractor()
    loop = asyncio.get_event_loop()
    tasks = []
    procex = ProcessPoolExecutor(max_workers=nprocs - 1)
    threx = ThreadPoolExecutor(max_workers=nprocs)
    for i, entry in enumerate(folder.glob("*.jpg")):
        tasks.append(
            loop.create_task(
                conv_file(
                    loop,
                    entry,
                    extr,
                    output=out,
                    procex=procex,
                    threx=threx,
                    show_warnings=show_warnings,
                ),
                name=f"task_convert-{str(entry)}",
            )
        )
    await asyncio.gather(*tasks, return_exceptions=True)


def convert_all_in_folder(
    folder: str_or_path,
    output: str = "both",
    show_warnings: bool = True,
    nprocs: int = NPROCS,
):
    """Convert all FLIR jpeg files in folder to csv and/or .mat (MATLAB) files.

    This will create a folder for each specified output option
    ('mat', 'csv_json') at the same level as the input folder.

    Parameters
    ----------
    folder : str or Path
    output : str
        Output format. One of 'text', 'mat' or 'both'.
    show_warnings : bool, optional
        Whether to show warning messages. Default is `True`, but they tend to
        be many, ant it is always the same...
    nprocs : int, optional
        Number of processes or threads to allow on the eventloop at once.
        Default is the logical number of processors on the machine.
    """
    aiorun(convert_all(folder, output, show_warnings, nprocs))


if __name__ == "__main__":
    import time

    # import ircamt
    t0 = time.time()
    # convert_all_in_folder('../data/jpg', show_warnings=False)
    convert_file("../data/jpg/IR_6512.jpg", show_warnings=False)
    print(f"Duration: {time.time() - t0:.3f} s")
