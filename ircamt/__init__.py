"""Helper tools to analyze IR (FLIR?) image sets."""

import ircamt.set_processing

from ircamt.process_temperatures import (
    make_timeline,
    datetime_from_flirstr,
    get_csvs,
    get,
    get_selection,
    max_temp,
    area_from_corners,
    integrate_ds,
    sequence_plot,
)
from ircamt.camera import Camera, ThermacamSc640, Scene

try:
    from flirimageextractor import FlirImageExtractor

    del FlirImageExtractor
    from ircamt.convert_flir import convert_all_in_folder, convert_file
except ModuleNotFoundError:
    pass
