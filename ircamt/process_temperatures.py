from datetime import datetime, timedelta
from pathlib import Path
from typing import Union, Callable, List, Tuple
from string import Template
import json
import numpy as np
from numpy.typing import NDArray, ArrayLike
import matplotlib.pyplot as plt
from matplotlib import animation

from twodpg import image_index
from ircamt.set_processing import NPROCS, set_processing

ProcOrThread, Queue = set_processing()

str_or_list = Union[str, list]
str_or_path = Union[str, Path]
arr_or_tuple = Union[NDArray, tuple]
int_or_tuple = Union[int, tuple]
seltp = Union[NDArray, List[NDArray], Tuple[NDArray]]

NAN = np.nan


def datetime_from_flirstr(flirstr: str) -> datetime:
    """Convert a time stamp from FLIR metadata format to Python datetime object.

    Parameters
    ----------
    flirstr : str
        Time stamp to convert.
    """
    dts, ts = flirstr.split(" ")
    ts, end = ts.split(".")
    y, mm, d = dts.split(":")
    h, m, s = ts.split(":")
    # The FLIR time stamps have 3 digits 'precision' after the second
    # the remainder after the '-' is something I don't understand
    us = float("0." + end.split("-")[0]) * 1e6
    y, mm, d, h, m, s, us = [int(el) for el in (y, mm, d, h, m, s, us)]
    return datetime(y, mm, d, h, m, s, us)


def make_timeline(
    src: str_or_list, units: str = "s", ret_paths: bool = False
) -> arr_or_tuple:
    """Create a time array from a bunch of JSON metadata files.

    Parameters
    ----------
    src : str or list
        Source specifier: Either a folder that contains .json metadata files
        or a list of paths pointing to JSON files.
    units : str
        Either 's' or 'datetime'.
        If 's', time is returned as seconds after the first frame.
        If 'datetime', time is returned as datetime objects.
    ret_paths : bool, optional
        Whether to return paths from which time was read. If `ret_path` is
        not passed `src` determins the behavior: if `src` is a folder, paths
        are returned; if `src` is a list, it is assumed that paths are known
        and are not returned.

    Returns
    -------
        array or tuple
    The timeline and optionally the corresponding file paths from which each
    time point was read.
    """
    if isinstance(src, str):
        src = Path(src)
    if isinstance(src, Path):
        jsons = list(src.glob("*.json"))
        jsons.sort()
    else:
        if (not isinstance(src[0], str)) and (not isinstance(src[0], Path)):
            raise TypeError(f"src has to be some type of path or string, got {src=}.")
        if isinstance(src[0], str):
            jsons = [Path(s) for s in src if s.endwith(".json")]
        else:
            jsons = [p for p in src if p.suffix == ".json"]
    t = []
    with open(jsons[0], "r") as fd:
        t0 = datetime_from_flirstr(json.load(fd)["DateTimeOriginal"])
    seconds = units == "s"
    for p in jsons:
        with open(p, "r") as fd:
            tm = datetime_from_flirstr(json.load(fd)["DateTimeOriginal"])
            if seconds:
                tm = timedelta.total_seconds(tm - t0)
            t.append(tm)
    return np.asarray(t), jsons if ret_paths else np.asarray(t)


def sequence_plot(
    src: list,
    title: str = "",
    timing: float = 1,
    action: str = "save_html",
    dpi: int = 150,
    max_filesize: float = 20.0,
):
    """Create an animated plot from a given sequence of files (currently only
     csv files).

    Parameters
    ----------
    src : list of Path or str
        Data source files to use for the sequence. The list will be sorted by
        filename.
    title : str
        Title to be placed at the top. This will also be used for the optional
        html export.
    timing : float
        Time (seconds) each frame will be shown.
    action : str
        One of "save_html", "return_anm"
    dpi: int
        Frame resolution.
    max_filesize: float
        Maximum size of images embedded in the html file. In Megabytes.

    Returns
    -------
    Depending on the action setting returns either `None` or the tuple
    (fig, ani)` which are instances of the figure and animation objects.
    """

    def analyze(arr):
        # 95.4
        iidx = image_index(*arr.shape)
        vidx = ~np.isnan(arr[iidx[:, 0], iidx[:, 1]])
        if np.any(vidx):
            sel = arr[iidx[vidx, 0], iidx[vidx, 1]]
            mx, mn = np.max(sel), np.min(sel)
            locmx, locmn = iidx[vidx][sel.argmax()], iidx[vidx][sel.argmin()]
            mean, ts = sel.mean(), np.percentile(sel, 99.7)
        else:
            mx, mn, locmx, locmn, mean, ts = [NAN, NAN, [-1, -1], [-1, -1], NAN, NAN]
        return mx, mn, locmx, locmn, mean, ts

    src = [Path(s) for s in src]
    src.sort()
    with open(src[0].parent / (src[0].stem + ".json")) as fp:
        t0 = datetime_from_flirstr(json.load(fp)["DateTimeOriginal"])

    labelstr = Template(
        r"\noindent Image size: $$ $w \times $h $$\\ "
        r"Number of pixels: $$ $totalpx$$; $$ $nnan $$ not valid, "
        r"$$ $nvalid$$ valid $$($valperc\%)$$\\ "
        r"Time: $$ $time\,\mathrm{s}$$\\ "
        r"$$T_\text{max}=$tempmax\,\mathrm{^\circ C}$$, $$ ($maxx, $maxy) $$\\ "
        r"$$T_\text{min}=$tempmin\,\mathrm{^\circ C}$$, $$ ($minx, $miny) $$"
    )

    def update(frname, img, txt, mxmrk, mnmrk, clb):
        with open(frname.parent / (frname.stem + ".json")) as fp:
            md = json.load(fp)
        t = (datetime_from_flirstr(md["DateTimeOriginal"]) - t0).total_seconds()
        dta = np.loadtxt(frname, delimiter=",")
        h, w = dta.shape
        mx, mn, locmx, locmn, avg, twosgm = analyze(dta)
        idx = ~np.isnan(dta)
        nvalid = np.count_nonzero(idx)
        nnan = w * h - nvalid
        txt.set_text(
            labelstr.substitute(
                w=w,
                h=h,
                totalpx=str(w * h),
                nvalid=nvalid,
                nnan=nnan,
                valperc=f"{100 * nvalid / (w * h):.0f}",
                time=f"{t:.0f}",
                tempmax=f"{mx:.0f}",
                maxx=locmx[1],
                maxy=locmx[0],
                tempmin=f"{mn:.0f}",
                minx=locmn[1],
                miny=locmn[0],
            )
        )
        mxmrk.set_data(*locmx[::-1, None])
        mnmrk.set_data(*locmn[::-1, None])
        img.set(array=dta, norm=plt.Normalize(vmin=0.0, vmax=twosgm))
        return img, txt, mxmrk, clb

    fig = plt.figure(figsize=(12, 6), dpi=dpi)
    ax = fig.add_axes((0.01, 0.05, 0.6, 0.9))
    ax.set(xlabel=r"x (column) / px", ylabel=r"y (row) / px", title=title)
    fl = Path(src[0])
    data = np.loadtxt(fl, delimiter=",")
    mx, mn, lmx, lmn, avg, twosgm = analyze(data)
    norm = plt.Normalize(vmin=0.0, vmax=twosgm)
    img = ax.imshow(data, norm=norm, cmap=plt.cm.cividis)
    (maxmark,) = plt.plot(*lmx[::-1], lw=0, marker="x", ms=5, color="red")
    (minmark,) = plt.plot(*lmn[::-1], lw=0, marker="x", ms=5, color="cyan")
    tlabel = ax.text(1.05, 1, f"t = 0 s", ha="left", va="top", transform=ax.transAxes)
    clb = plt.colorbar(
        img, ax=ax, location="left", shrink=0.75, label=r"$\mathrm{^\circ C}$"
    )

    ani = animation.FuncAnimation(
        fig,
        update,
        src,
        fargs=(img, tlabel, maxmark, minmark, clb),
        interval=timing,
        repeat=False,
    )
    if action == "return_anm":
        return fig, ani
    if action == "save_html":
        plt.rc("animation", embed_limit=max_filesize)
        if timing > 1:
            fps = 1
        else:
            fps = int(np.round(1.0 / timing, decimals=0))
        anil = ani.to_jshtml(fps=fps, embed_frames=True).split("\n")
        # This is currently a bad hack (but works...). Probably need to work
        # with one of the animation.Writer thingies.
        anil[0] = "<html><head>\n"
        anil[181] += ".animation img { width: %din; }" % fig.get_size_inches()[0]
        anil[183] = "</head><body>"
        anil[-1] = "</body></html>"
        fname = title.replace(" ", "_") + "_flir-temps.html"
        with open(fname, mode="w") as fd:
            fd.write("\n".join(anil))


def get_selection(sel: seltp = None, file: str_or_path = None) -> List[NDArray]:
    """Load an image from a csv file and return selected region.

    Parameters
    ----------
    sel : (Nx2) NDArray or tuple thereof or list thereof
        Coordinates of the image pixels to select/return. If None, values of
        all coordinates are returned.
    file : str or Path
        File path to read from.
    """
    temp = np.loadtxt(file, delimiter=",", dtype=np.float64)
    if sel is None:
        sel = (image_index(*temp.shape),)
    return [temp[selel[:, 0], selel[:, 1]] for selel in sel]


def get_csvs(
    files: list = None, fold: str_or_path = None, start: int = None, end: int = None
) -> list:
    """Get a list of csv files from a list of file paths or a folder.

    Parameters
    ----------
    files : list
        File paths to read from. If paths end in '.json' a corresponding '.csv'
        file will be searched and loaded.
    fold : str or Path
        Folder to read from. All JSON files will be listed and sorted by name.
        `start` and `end` can be used to select a subset.
    start : int
        Index from where to start reading. This refers to the index in the file
        list and not to a potential frame number in file names.
    end : int
        Index where to stop reading.
    """
    if files is not None:
        csvs = []
        for p in files:
            p = Path(p)
            csvs.append(p.parent / (p.stem + ".csv"))
    else:
        csvs = list(Path(fold).glob("*.csv"))
        csvs.sort()
        if start is not None:
            csvs = csvs[start:end]
    return csvs


def work_on_chunk(
    chunk: list,
    que: Queue,
    start: int,
    end: int,
    sel: NDArray,
    what: Callable,
    args: tuple,
    iterargs: list,
    nrvals: int,
):
    """A worker function. Don't call directly."""
    shp = (len(sel), len(chunk)) if nrvals == 1 else (len(sel), len(chunk), nrvals)
    ret = np.empty(shp)
    for i, file in enumerate(chunk):
        tsels = get_selection(sel, file)
        zipargs = [tsels] + [args[i] for i in iterargs]
        for j, rg in enumerate(zip(*zipargs)):
            if len(rg) > 1:
                aargs = rg[1:] + tuple(
                    [args[k] for k in range(len(args)) if k not in iterargs]
                )
                rt = what(rg[0], *aargs)
                ret[j, i] = rt
            else:
                ret[j, i] = what(rg[0], *args)
    que.put((ret, start, end))


def get(
    what: Callable,
    files: list,
    sel: seltp = None,
    whatargs: tuple = (),
    iterargs: list = [],
    nrvals: int = 1,
    nprocs: int = NPROCS,
) -> NDArray:
    """Apply the function `what` to each of the images given in the `files` list;
    `sel` specifies the subset of an image to use.

    Parameters
    ----------
    what : function
        What to compute from each image/selection. E.g. maximum, average...
        Must return one or several values. If more than one value is returned,
        the `nrvals` parameter must specify how many.
        For example the max_temp function returns the maximum temperature and
        row and column index where it was found. In that case `nrvals=3`.
    files : list
        File paths to read from.
    sel : (Nx2) NDArray, or a list of such arrays.
        y, x pairs of image coordinates to analyze.
        Also, a list of array sets can be passed. Each set of coordinate pairs
        will be passed to the `what()` function.
    whatargs : tuple
        arguments of `what`.
    iterargs : list
    nrvals : int, optional
        Specifies the number of values returned by `what`. If not specified, 1
        is assumed.
    nprocs : int
        Number of proccesses or threads to use. There is at most one thread
        per image.

    Returns
    -------
    Values returned by the `what` function from all sources listed in `files`.
    """
    single = isinstance(sel, np.ndarray)  # Check if sel is a single set of
    if single:  # selection coordinates
        single = sel.ndim < 3 and sel.shape[1] < 3
    if single:
        ln = 1
        sel = (sel,)
    else:
        ln = len(sel)
    shp = (len(files),) if nrvals == 1 else (len(files), nrvals)
    ret = np.empty((ln,) + shp)
    if len(files) < nprocs:
        nprocs = len(files)
    que = Queue()
    itms_per_proc = len(files) // nprocs
    start, end, prcs = 0, -1, []
    for k in range(nprocs):
        end = start + itms_per_proc
        args = (
            files[start:end],
            que,
            start,
            end,
            sel,
            what,
            whatargs,
            iterargs,
            nrvals,
        )
        p = ProcOrThread(target=work_on_chunk, args=args)
        prcs.append(p)
        start = end
        p.start()
    args = (
        files[start:],
        que,
        start,
        len(files),
        sel,
        what,
        whatargs,
        iterargs,
        nrvals,
    )
    p = ProcOrThread(target=work_on_chunk, args=args)
    prcs.append(p)
    p.start()
    for _ in range(len(prcs)):
        el = que.get()
        try:
            chunk, start, end = el
        except ValueError:
            print(f"{el=}")
            raise
        j = -1
        try:
            for j in range(ln):
                ret[j, start:end] = chunk[j]
        except ValueError:
            print(f"{j=}, {start=}, {end=},\n{chunk=}")
            raise
    for p in prcs:
        p.join()
    return ret[0] if single else ret


def max_temp(tmps):
    """Minimalistic example of a temperature processing function. Better you
    write your own.
    """
    idx = ~np.isnan(tmps)
    if np.any(idx):
        mx = np.max(tmps[idx])
    else:
        mx = np.nan
    return mx


def area_from_corners(corners: NDArray, shape: str = "trapezoid") -> NDArray:
    """Compute N areas from 4N or 3N corner coordinates.

    Parameters
    ----------
    corners : 4Nx3 or 3Nx3 array
        Corner coordinates are assumed to occur in groups of four or three
        in this array:
        [corner1, corner2, corner3, corner4,
         corner1, corner2, corner3, corner4,...]
    shape : str
        The shape that `corners` enclose. Either 'trapezoid' or 'triangle'.

    Returns
    -------
    N areas (their seizes, but not their orientations).
    """
    if shape == "trapezoid":
        skip = 4
    elif shape == "triangle":
        skip = 3
    else:
        raise ValueError(
            f"'shape' must be either 'trapezoid' or 'triangle, but got {shape=}."
        )
    corners = np.asarray(corners)
    crn1, crn2, crn3 = corners[0::skip], corners[1::skip], corners[2::skip]
    areas = 0.5 * np.sqrt(np.sum(np.cross(crn2 - crn1, crn3 - crn1) ** 2, axis=1))
    if shape == "trapezoid":
        crn4 = corners[3::4]
        areas += 0.5 * np.sqrt(np.sum(np.cross(crn3 - crn1, crn4 - crn1) ** 2, axis=1))
    return areas


def integrate_ds(
    x: ArrayLike,
    /,
    *,
    area: ArrayLike = None,
    corners: ArrayLike = None,
    ret_area: str = "no",
) -> arr_or_tuple:
    """Integrate `x` over given area elements. Either an area array, or a corners
    array have to be given. If `area` is passed, `corners` is ignored. If none
    of the two is passed, all area elements are assumed as 1, and the result is
    sum(x).

    Parameters
    ----------
    x : ArrayLike
        The thing to integrate.
    area : ArrayLike
        Area elements to use.
    corners : ArrayLike
        Vertice coordinates of the corners of each area element. Area elements
        may have three or four corners.
    ret_area : str
        Whether to return area, and if so, what to return. 'no': do not return
        area info. 'sum': return the total area. 'all': return all area elements.

    Returns
    -------

    """
    x = np.asarray(x)
    corners = np.asarray(corners)
    if area is None and corners is None:
        area = np.ones_like(x)
    if area is not None:
        assert len(area) == len(
            x
        ), f"x and area must have same length, but got {len(x)=} and {len(area)=}."
    else:
        lnt, lnc = len(x), len(corners)
        assert (
            lnc == 4 * lnt or lnc == 3 * lnt
        ), f"Cannot interpret corner array. Got {len(x)=} and {len(corners)=}."
        area = area_from_corners(corners)
    ret = np.sum(x * area)
    if ret_area == "no":
        return ret
    elif ret_area == "sum":
        return ret, area.sum()
    elif ret_area == "all":
        return ret, area
    else:
        raise ValueError(
            f"ret_area must be one of 'no', 'sum', 'all'; but got {ret_area=}."
        )
