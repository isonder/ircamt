"""Apply measured temperatures to real space, not only camera pixels.
"""
import numpy as np
from numpy.typing import NDArray, ArrayLike
import cv2

NAN = np.nan


class Camera:
    """A container that holds a number of attributes specific to a camera, and
    provides the intrinsic matrix necessary for coordinate transforms.
    """

    lense_type = "pinhole camera"
    manuf = "unknown brand"
    model = "no model"
    sens_size_x_px: float = NAN
    sens_size_y_px: float = NAN
    center_x: float = sens_size_x_px / 2
    center_y: float = sens_size_y_px / 2
    sensor_pitch_m = NAN
    dist_coeffs: NDArray = np.zeros(5, dtype=float)

    def __init__(self, name: str = "noname", f_mm: float = NAN):
        """

        Parameters
        ----------
        name : str
            Some name for a camera.
        f_mm : float
            The camera's focal length in mm.
        """
        self.name: str = name
        assert (
            f_mm * 1e-3 / self.sensor_pitch_m > 1.0
        ), f"Camera.init: cannot create camera with focal_length {f_mm=}"
        self._fmm: float = f_mm
        self._fpx: float = NAN

    @property
    def f_px(self) -> float:
        """Focal length in pixels."""
        if np.isnan(self._fpx):
            self._fpx = self._fmm * 1e-3 / self.sensor_pitch_m
        return self._fpx

    @f_px.setter
    def f_px(self, val: float):
        assert val > 1, f"Camera.f_px.setter: cannot set focal_length to {val=} px."
        if val != self._fpx:
            self._fmm = NAN
        self._fpx = val

    @property
    def f_mm(self) -> float:
        """Focal length in mm."""
        if np.isnan(self._fmm):
            self._fmm = self._fpx * self.sensor_pitch_m * 1e3
        return self._fmm

    @f_mm.setter
    def f_mm(self, val: float):
        assert (
            val * 1e-3 / self.sensor_pitch_m > 1.0
        ), f"Camera.f_mm.setter: cannot set focal_length tp {val=} mm."
        if val != self._fmm:
            self._fpx = NAN
        self._fmm = val

    @property
    def intrinsic_matrix(self) -> NDArray:
        return np.array(
            [
                [self.f_px, 0.0, self.center_x],
                [0.0, self.f_px, self.center_y],
                [0.0, 0.0, 1.0],
            ]
        )

    @property
    def cmi(self) -> NDArray:
        """Inverse of the intrinsic matrix."""
        oof = 1.0 / self.f_px
        return np.array(
            [
                [oof, 0.0, -self.center_x * oof],
                [0.0, oof, -self.center_y * oof],
                [0.0, 0.0, 1.0],
            ]
        )

    def _intr_matrix_str(self) -> str:
        return (
            r"A=\begin{pmatrix}"
            f"\n{self.f_px:.0f} & 0 & {self.center_x:.0f} \\\\\n"
            f"0 & {self.f_px:.0f} & {self.center_y:.0f} \\\\\n"
            f"0 & 0 & 1\n"
            r"\end{pmatrix}"
        )

    def _repr_html_(self) -> str:
        name_s = f"{self.name}: " if self.name != "noname" else ""
        s = (
            f"<div>\n"
            f"  <p>{name_s}{self.manuf}, {self.model}\n"
            f"      <code>[{self.lense_type}]</code></p>\n"
            f"  <ul>\n"
            f"      <li>sensor: ({self.sens_size_x_px} px, {self.sens_size_y_px} px)</li>"
            f"      <li>focal length: {self.f_px} px</li>\n"
            f"  </ul>\n"
            f"<p>Intrinsic matrix [px]:</p>\n"
            f"$$\n{self._intr_matrix_str()}\n$$</div>"
        )
        return s


class ThermacamSc640(Camera):
    manuf = "FLIR Systems"
    model = "ThermaCAM SC640"
    sens_size_x_px: float = 640
    sens_size_y_px: float = 480
    center_x: float = sens_size_x_px / 2 - 1
    center_y: float = sens_size_y_px / 2 - 1
    # The datasheet mentions a sensor pitch of 50 micro meters, but that value
    # results in systematically wrong transformations.
    sensor_pitch_m = float = 1e-4 / 3


class Scene:
    """An arrangement of a camera and a `world` coordinate system that contains
    some coordinate transformation information.
    """

    def __init__(self, cam: Camera, rot: ArrayLike, trans: ArrayLike):
        """

        Parameters
        ----------
        cam : Camera
            A `Camera` instance.
        rot : 1D or 2D array like
            Rotation vector or matrix as returned by `cv2.solvePnP()`
        trans : 1D array like
            Translation vector as returned by `cv2.solvePnP()`
        """
        self.cam: Camera = cam
        rot = np.asarray(rot)
        assert rot.ndim > 0, f"Cannot initialize scene with rotation {rot=}."
        if rot.ndim == 1 or rot.shape == (3, 1):
            self.rot_v: NDArray = np.asarray(rot)
            self.rot_m: NDArray = cv2.Rodrigues(rot)[0]
        else:
            self.rot_m: NDArray = np.asarray(rot)
            self.rot_v: NDArray = cv2.Rodrigues(rot)[0]
        self.trans: NDArray = np.asarray(trans)
        self.camloc: NDArray = -np.dot(
            self.rot_m.T, self.trans - np.array([0.0, 0.0, self.cam.f_mm * 1e-3])
        )

    def world_to_cam(self, xw: NDArray) -> NDArray:
        """Transform world coordinates to camera coordinates.

        Parameters
        ----------
        xw : NDArray
            World coordinates to transform.

        Returns
        -------
        Given points projected into camera coordinates (2D).
        (NOTE: The `cv2.projectPoints` return format is somewhat weird, since it
        has an additional axis which I don't quite get. For now this axis is
        filtered out, and th hope is that this just works...)
        """
        return cv2.projectPoints(
            xw,
            self.rot_v,
            self.trans,
            self.cam.intrinsic_matrix,
            self.cam.dist_coeffs,
        )[0][:, 0]

    def cam_to_worldplain(
        self, normal: ArrayLike, p0: ArrayLike, uv: ArrayLike
    ) -> NDArray:
        """Transform 2D camera coordinates into world coordinates, assuming
        the points are part of a plain with the given surface `normal`, and a
        reference point `p0`.
        STATUS: It seems to work, but could profit from more rigorous testing.

        Parameters
        ----------
        normal : ArrayLike
            The plain's normal vector in world coordinates.
        p0 : ArrayLike
            Reference point that is part of the plain.
        uv : ArrayLike
            Image (or camera) relative coordinates to transform. Ths is assumed
            to be either an x,y coordinate pair or an Nx2 shaped array of such.

        Returns
        -------
        The corresponding points in the 3D world coordinate system.
        """
        normal, p0, uv = np.asarray(normal), np.asarray(p0), np.asarray(uv)
        if uv.ndim < 2:
            ln, ret_single = (1, True)
            uv = np.array([uv])
        else:
            ln, ret_single = len(uv), False
        uv1 = np.empty((ln, 3))
        uv1[:, :2] = uv[:, :2]
        uv1[:, 2] = 1.0
        x = np.dot(self.rot_m.T, np.dot(self.cam.cmi, uv1.T))
        xx0 = np.dot(self.rot_m.T, self.trans)
        ret = np.transpose(np.dot(normal, p0 + xx0) * x / np.dot(normal, x)) - xx0
        return ret[0] if ret_single else ret

    def cam_to_worldpoint(self, zc: ArrayLike, uv: ArrayLike) -> NDArray:
        """Given a known distance, `zc`, from camera to a point of interest,
        returns the 3D world coordinates of that point.

        Parameters
        ----------
        zc : ArrayLike
            Camera distances.
        uv : ArrayLike
            Image coordinates. assumed to be either an x,y coordinate pair or
            an Nx2 shaped array of such.

        Returns
        -------
            An Nx3 array of world coordinates.
        """
        try:
            ln, ret_single = len(zc), False
        except TypeError:
            ln, ret_single = 1, True
            zc = np.array([zc])
        uv = np.asarray(uv)
        assert (
            uv.shape[1] == 2
        ), f"Scene.cam_to_worldpoint: Got wrong shaped uv array: {uv.shape=}"
        uv1 = np.empty((ln, 3))
        uv1[:, :2] = uv[:, :]
        uv1[:, 2] = 1.0
        ret = np.dot(self.rot_m.T, zc * np.dot(self.cam.cmi, uv1.T)).T
        return ret[0] if ret_single else ret

    def _repr_html_(self) -> str:
        s = "\n".join(
            [
                "<div>Scene:\n",
                '  <p style="padding-left: 2ex;">' "    Camera at \n$$",
                r"\pmb{x}_\text{cam,w} = "
                r"(%.3f\,\mathrm{m}, %.3f\,\mathrm{m}, %.3f\,\mathrm{m})"
                % tuple(self.camloc),
                "$$<br/>\n   Rotation: \n$$",
                r"R=\begin{pmatrix}",
                f"{self.rot_m[0, 0]:.6f} & {self.rot_m[0, 1]:.6f} & {self.rot_m[0, 2]:.6f} \\\\",
                f"{self.rot_m[1, 0]:.6f} & {self.rot_m[1, 1]:.6f} & {self.rot_m[1, 2]:.6f} \\\\",
                f"{self.rot_m[2, 0]:.6f} & {self.rot_m[2, 1]:.6f} & {self.rot_m[2, 2]:.6f}",
                r"\end{pmatrix}",
                "$$\n    Translation: \n$$",
                r"\pmb{x}_0=(%.3f\,\mathrm{m}, %.3f\,\mathrm{m}, %.3f\,\mathrm{m})"
                % tuple(self.trans),
                "$$\n  </p>",
                "</div>",
            ]
        )
        return s
